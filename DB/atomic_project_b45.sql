-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2017 at 06:55 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b45`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `birthDate` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `name`, `birthDate`, `softDelete`) VALUES
(1, 'Sumon', '1997-07-18', 'No'),
(5, 'AB Akas', '1997-11-20', 'No'),
(8, 'AB Akas', '1997-11-20', 'No'),
(9, 'AB Akas', '2017-02-25', 'Yes'),
(11, 'sagla', '2017-02-23', 'No'),
(12, 'Samrat Barua', '2000-07-08', 'Yes'),
(13, 'leda poa', '2017-02-11', 'No'),
(14, 'Paglu', '2017-02-15', 'No'),
(15, 'Saglu', '2017-02-10', 'No'),
(16, 'Montu', '2017-02-10', 'No'),
(17, 'Lover Boy Sumon', '2012-07-06', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(2, 'Book Name', 'Author Name', 'Yes'),
(7, 'AB', 'ABAkas', 'No'),
(12, 'ab', 'AB', 'Yes'),
(13, 'AB', 'My Name', 'No'),
(14, 'Story', 'Sumon', 'Yes'),
(15, 'AB', 'AB Akas', 'No'),
(16, 'Story', 'Sumon', 'Yes'),
(17, 'Story', 'Sumon', 'Yes'),
(18, 'Story', 'Sumon', 'No'),
(19, 'AB', 'ABAkas', 'No'),
(20, 'mn,dnv', 'jsdnkv', 'Yes'),
(21, 'mn,dnv', 'jsdnkv', 'Yes'),
(23, 'mn,dnv', 'jsdnkv', 'No'),
(24, 'Prodip', 'No Name', 'No'),
(25, 'mn,dnv', 'jsdnkv', 'No'),
(26, 'book', 'author', 'No'),
(27, 'ab', 'AB', 'No'),
(28, 'AB', 'ABAkas', 'No'),
(30, 'AB', 'ABAkas', 'No'),
(31, 'Bangla', 'Kono ek lekok', 'No'),
(32, 'Shurjha', 'Jani na', 'No'),
(33, 'Himaloy Barua', 'AB Akas', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `cityName` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `cityName`, `softDelete`) VALUES
(1, 'AB Akas', 'Chittagong', 'No'),
(4, 'Pagla', 'Chittagong', 'No'),
(6, 'Saglu', 'Khulna', 'No'),
(7, 'Samrat', 'Dhaka', 'Yes'),
(8, 'Sumon', 'Jossore', 'No'),
(9, 'Sagu', 'Chittagong', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `softDelete`) VALUES
(1, 'Samrat', 'Samrat123@gmail.com', 'No'),
(2, 'Sumon', 'sumon@gmail.com', 'Yes'),
(3, 'Sagu', 'Sagu@yahoo.com', 'Yes'),
(4, 'Pagla', 'pagla@gmail.com', 'No'),
(5, 'Saglu', 'saglu@gmail.com', 'No'),
(7, 'Anowar', 'Anowar@gmail.com', 'Yes'),
(11, 'Pagla', 'pagla@gmail.com', 'No'),
(12, 'AB Akas', 'ab.akas@yahoo.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `softDelete`) VALUES
(1, 'Pagli', 'Female', 'No'),
(4, 'Sumon', 'Male', 'Yes'),
(5, 'Sumon', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobbies` varchar(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `softDelete`) VALUES
(1, 'Sumon', 'Reading,Drawing.', 'No'),
(2, 'shantu', 'Reading,Gardening,Singing.', 'No'),
(4, 'Paglu', 'Reading,Drawing,Swimming,Gardening,Singing.', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `pictureName` varchar(111) NOT NULL,
  `tmpLocation` varchar(111) NOT NULL,
  `type` varchar(111) NOT NULL,
  `size` int(111) NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `pictureName`, `tmpLocation`, `type`, `size`, `softDelete`) VALUES
(1, 'Sumon', '1486155111library shelves.jpg', 'C:\\xampp\\tmp\\php3BCE.tmp', 'image/jpeg', 112, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `description` text NOT NULL,
  `softDelete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
