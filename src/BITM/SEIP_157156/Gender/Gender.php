<?php

namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Gender extends DB{
    private $id;
    private $name;
    private $gender;
    private $btnValue;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("gender",$allPostData)){
            $this->gender = $allPostData['gender'];
        }
        if(array_key_exists("btn",$allPostData)){
            $this->btnValue = $allPostData['btn'];
        }
    }

    public function store()
    {
        $arrayData = array($this->name,$this->gender);
        $query = 'INSERT INTO gender (name, gender) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('create.php');
        }
        else{
            Utility::redirect('read.php');
        }
    }

    public function index(){
        $query = "SELECT * FROM gender WHERE softDelete = 'No'";
        $STH = $this->DBH->query($query);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view()
    {
        $sql = "SELECT * FROM gender WHERE id = '".$this->id."'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function update()
    {
        $arrayData = array($this->name,$this->gender,$this->id);
        $sql = "UPDATE gender SET name = ?, gender = ? WHERE id = ?";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Updated!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Updated!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('read.php');
        }
        else{
            Utility::redirect('trashed.php');
        }
    }

    public function trash()
    {
        $arrayData = array("Yes",$this->id);
        $query = 'UPDATE gender SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Trashed!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!.");
        }

        Utility::redirect('read.php');
    }

    public function delete()
    {
        $arrayData = array($this->id);
        $query = "DELETE FROM gender WHERE id = ?";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted Permanently.!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Delete!.");
        }

        if($this->btnValue == "set")   Utility::redirect('read.php');
        else Utility::redirect('trashed.php');
    }

    public function trashed()
    {
        $sql = "select * from gender WHERE softDelete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function recover()
    {
        $arrayData = array("No",$this->id);
        $query = 'UPDATE gender SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!.");
        }

        Utility::redirect('trashed.php');
    }
}