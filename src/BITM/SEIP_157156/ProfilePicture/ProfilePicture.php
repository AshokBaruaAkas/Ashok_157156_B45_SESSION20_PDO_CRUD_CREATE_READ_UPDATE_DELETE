<?php

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class ProfilePicture extends DB{
    private $id;
    private $name;
    private $pictureName;
    private $pictureTemLocation;
    private $pictureType;
    private $pictureSize;
    private $btnValue;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("btn",$allPostData)){
            $this->btnValue = $allPostData['btn'];
        }
    }

    public function setProfilePicture($allFileData = null){
        if(array_key_exists("picture",$allFileData)){
            $this->pictureName = time().$allFileData['picture']['name'];

            $this->pictureTemLocation = $allFileData['picture']['tmp_name'];

            $this->pictureType = $allFileData['picture']['type'];

            $this->pictureSize = (($allFileData['picture']['size'])/1024);
        }
    }

    public function storePicture()
    {
        move_uploaded_file($this->pictureTemLocation,"UploadedPicture/".$this->pictureName);
    }

    public function store()
    {
        $arrayData = array
        (
            $this->name,
            $this->pictureName,
            $this->pictureTemLocation,
            $this->pictureType,
            $this->pictureSize
        );
        $query = 'INSERT INTO profilePicture (name, pictureName, tmpLocation, type, size) VALUES (?,?,?,?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('create.php');
        }
        else{
            Utility::redirect('read.php');
        }
    }

    public function index(){
        $query = "SELECT * FROM profilePicture WHERE softDelete = 'No'";
        $STH = $this->DBH->query($query);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view()
    {
        $sql = "SELECT * FROM profilepicture WHERE id = '".$this->id."'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function update()
    {
        $arrayData = array
        (
            $this->name,
            $this->pictureName,
            $this->pictureTemLocation,
            $this->pictureType,
            $this->pictureSize,
            $this->id
        );
        $sql = "UPDATE profilePicture SET name = ?, pictureName = ?, tmpLocation = ?, type = ?, size = ? WHERE id = ?";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Updated!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Updated!.");
        }

        if($this->btnValue == "set"){
            Utility::redirect('read.php');
        }
        else{
            Utility::redirect('trashed.php');
        }
    }

    public function trash()
    {
        $arrayData = array("Yes",$this->id);
        $query = 'UPDATE profilePicture SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Trashed!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!.");
        }

        Utility::redirect('read.php');
    }

    public function delete()
    {
        $arrayData = array($this->id);
        $query = "DELETE FROM profilePicture WHERE id = ?";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted Permanently.!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Delete!.");
        }

        if($this->btnValue == "set")   Utility::redirect('read.php');
        else Utility::redirect('trashed.php');
    }

    public function trashed()
    {
        $sql = "select * from profilepicture WHERE softDelete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function recover()
    {
        $arrayData = array("No",$this->id);
        $query = 'UPDATE profilePicture SET softDelete = ? WHERE id = ?';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered!.");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!.");
        }

        Utility::redirect('trashed.php');
    }
}