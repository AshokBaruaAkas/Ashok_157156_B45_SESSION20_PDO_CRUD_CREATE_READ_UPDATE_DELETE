<?php
require_once("../../../vendor/autoload.php");

$objGender = new \App\Gender\Gender();

$objGender->setData($_GET);

$oneData = $objGender->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender - Single Birth Day Information</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Single Gender Information</h1>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <div class="row col-md-3"></div>
            <div class="row col-md-7">
                <table class="table table-bordered table table-striped">
                    <tr>
                        <th width="30%">Name</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                        <td>ID:</td>
                        <td><?php echo $oneData->id;?></td>
                    </tr>
                    <tr>
                        <td>User Name:</td>
                        <td><?php echo $oneData->name;?></td>
                    </tr>
                    <tr>
                        <td>Gender:</td>
                        <td><?php echo $oneData->gender;?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
</body>
</html>