<?php
require_once("../../../vendor/autoload.php");

$objGender = new \App\Gender\Gender();

$allData = $objGender->index();

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
if(!isset($msg)){
    $msg = Message::getMessage();
}
else{
    $msg = "";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender - Active List</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Gender - Active List</h1>
        <div class="notification">
            <div class="message text-center">
                <h3><?php echo $msg;?></h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"  class="active"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <table class="table table-striped table-bordered">
                <tr>
                    <th width="10%">Sl No</th>
                    <th width="10%">ID</th>
                    <th>User Name</th>
                    <th>Gender</th>
                    <th width="25%">Action Buttons</th>
                </tr>
                <?php
                $Serial = 1;
                foreach($allData as $oneData){
                    echo "
                    <tr>
                        <td>$Serial</td>
                        <td>$oneData->id</td>
                        <td>$oneData->name</td>
                        <td>$oneData->gender</td>
                        <td>
                            <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                            <a href='edit.php?id=$oneData->id&btn=set' class='btn btn-success'>Edit</a>
                            <a href='trash.php?id=$oneData->id' class='btn btn-warning'>Trash</a>
                            <a href='delete.php?id=$oneData->id&btn=set' class='btn btn-danger'>Delete</a>
                        </td>
                    </tr>
                ";
                    $Serial++;
                }
                ?>
            </table>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>