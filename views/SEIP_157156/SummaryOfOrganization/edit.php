<?php
require_once("../../../vendor/autoload.php");

$objSummaryOfOrganization = new \App\SummaryOfOrganization\SummaryOfOrganization();

$objSummaryOfOrganization->setData($_GET);

$oneData = $objSummaryOfOrganization->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Organization Information</title>
    <link rel="stylesheet" href="../../../resource/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
<div class="main-content">
    <div class="head">
        <h1>Update Organization Information</h1>
    </div>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>
    <div class="site_body">
        <div class="container">
            <form action="update.php" method="post" class="form text-center">
                <div class="form-group">
                    <label for="bookName">Edit Name</label>
                    <input class="form-control text-center" type="text" name="name" value="<?php echo $oneData->name;?>">
                </div>
                <div class="form-group">
                    <label for="description">Enter Your Description</label>
                    <textarea class="form-control text-center" name="description" required><?php echo $oneData->description?></textarea>
                </div>
                <input type="hidden" name="id" value="<?php echo $oneData->id;?>">
                <input type="hidden" name="btn" value="<?php echo $_GET['btn'];?>">
                <button type="submit" class="btn btn-lg btn-primary">Update</button>
            </form>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
</body>
</html>